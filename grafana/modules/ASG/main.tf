resource "aws_launch_template" "launch_template" {
  name   = var.grafana_template
  image_id      = var.launch_ami
  instance_type = var.instance
}

resource "aws_autoscaling_group" "autoscaling" {
  availability_zones = var.availability_zones
  desired_capacity   = var.desired_capacity
  max_size           = var.max_size
  min_size           = var.min_size

  launch_template {
    id      = aws_launch_template.launch_template.id
    version = "$Latest"
  }
}