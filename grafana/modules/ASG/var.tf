variable "grafana_template" {
  type =   string
  description = "template name"  
}
variable "launch_ami" {
  type = string
  description = "launch_ami"  
}
variable "instance" {
  type = string
  description = "instance"  
}

variable "desired_capacity" {
  type = string
  description = "desired_capacity"  
}
variable "max_size" {
  type = string
  description = "template name"  
}
variable "min_size" {
  type = string
  description = "template name"  
}

variable "availability_zones" {
 type = list (string)

}

