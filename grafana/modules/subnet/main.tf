#pub
resource "aws_subnet" "Main1" {
  vpc_id     = var.vpc_ronit
  cidr_block = var.subnets_cidr
  map_public_ip_on_launch = true
  availability_zone       = var.av_zone_pub
  tags = {
    Name = var.tag1
  }
}
#private
resource "aws_subnet" "Main2" {
  count = length(var.subnets_cidr2)
  vpc_id     = var.vpc_ronit1
  cidr_block = var.subnets_cidr2[count.index]
  availability_zone  = var.av_zone_pri[count.index]
  tags = {
    Name = var.tag2[count.index]
  }
}