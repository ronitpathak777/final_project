#--------public subnet ----------#
variable "subnets_cidr" {
  description = "specify cidr block for vpc"
  type        = string
}

variable "tag1" {
  description = "specify tag"
  type        = string
}
variable "vpc_ronit" {
  description = ""
}

variable "av_zone_pub" {
  default     = ""
  description = "description"
}

#--------------pvt-subnet------#
variable "subnets_cidr2" {
  description = "specify cidr block for vpc"
  type        = list(string)
  default     = ["10.0.4.0/23", "10.0.6.0/23","10.0.7.0/23"]
}
variable "tag2" {
  description = "specify tag"
  type        = list(string)
  default     = ["ninja-priv-sub-01", "ninja-priv-sub-02"]
}
variable "vpc_ronit1" {
  description = "specify tag"
}
variable "av_zone_pri" {
  type = list(string)
  description = "description"
}
