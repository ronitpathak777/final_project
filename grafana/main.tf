module "vpc" {
  source           = "./modules/vpc"
  cidr_block       = var.cidr_block
  instance_tenancy = var.instance_tenancy
  tags             = var.tags
}
module "subnet" {
  source        = "./modules/subnet"
  subnets_cidr  = var.subnets_cidr
  tag1          = var.tag1
  av_zone_pub   = var.av_zone_pub
  vpc_ronit     = module.vpc.vpc_id
  subnets_cidr2 = var.subnets_cidr2
  tag2          = var.tag2
  av_zone_pri   = var.av_zone_pri
  vpc_ronit1    = module.vpc.vpc_id
}

module "igw-public" {
  source  = "./modules/igw-public"
  igw     = var.igw
  vpc_igw = module.vpc.vpc_id
}
module "route-table" {
  source                  = "./modules/route-table"
  rtb_name                = var.rtb_name
  routes                  = var.routes
  vpc_rtb                 = module.vpc.vpc_id
  igw_gateway             = module.igw-public.aws_internet_gateway
  pub_subnet_id           = module.subnet.aws_subnet
  nat_id                  = module.nat-gateway.nat_id
  private_route_table_tag = var.private_route_table_tag
  pri_subnet_id           = module.subnet.pri_subnet_id[*]
}

module "nat-gateway" {
  source    = "./modules/nat-gateway"
  eip_tag   = var.eip_tag
  subnet_id = module.subnet.aws_subnet
  nat_tag   = var.nat_tag
}
module "ec2" {
  source            = "./modules/ec2"
  pub_ami           = var.pub_ami
  pub_instance_type = var.pub_instance_type
  pub_subnet_id     = module.subnet.aws_subnet
  key_name          = var.key_name
  pri_ami           = var.pri_ami
  pub_instance_tag  = var.pub_instance_tag
  pri_instance_type = var.pri_instance_type
  pri_subnet_id     = module.subnet.pri_subnet_id[*]
  pri_instance_tag  = var.pri_instance_tag
  security_group_id = module.security-group.security_group_id[*]
}
module "security-group" {
  source        = "./modules/security-group"
  vpc_id        = module.vpc.vpc_id
  sg_tag        = var.sg_tag
  sg_cidr_block = var.sg_cidr_block
}
module "nacl" {
  source          = "./modules/nacl"
  vpc_id          = module.vpc.vpc_id
  protocol        = var.protocol
  rule_no         = var.rule_no
  action          = var.action
  nacl_cidr_block = var.nacl_cidr_block
  from_port       = var.from_port
  to_port         = var.to_port
  nacl_tag        = var.nacl_tag
  subnet_id       = module.subnet.pri_subnet_id[*]
}

module "ASG" {
  source        = "./modules/ASG"
 grafana_template  =  "grafana"
  launch_ami    =  "ami-08fdec01f5df9998f"
  instance      = "t2.micro"
  availability_zones = ["us-east-1b"]
  desired_capacity   = 2
  max_size           = 5
  min_size           = 1
}

