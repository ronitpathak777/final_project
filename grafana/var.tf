variable "cidr_block" {
  description = "specify cidr block for vpc"
  type        = string
  default     = "10.0.0.0/18"
}
variable "instance_tenancy" {
  description = "specify tenancy"
  type        = string
  default     = "default"
}
variable "tags" {
  description = "specify name"
  type        = string
  default     = "ninja-vpc-1"
}
#--------public subnet ----------#
variable "subnets_cidr" {
  description = "specify cidr block for vpc"
  type        = string
  default     = "10.0.0.0/23"
}

variable "tag1" {
  description = "specify tag"
  type        = string
  default     = "ninja-pub-sub-01"
}
variable "av_zone_pub" {
  default     = ""
  type        = string
  description = "description"
}

#--------------pvt-subnet------#
variable "subnets_cidr2" {
  description = "specify cidr block for vpc"
  type        = list(string)
  default     = ["10.0.4.0/23", "10.0.6.0/23"]
}
variable "tag2" {
  description = "specify tag"
  type        = list(string)
  default     = ["ninja-priv-sub-01", "ninja-priv-sub-02"]
}
variable "av_zone_pri" {
  type        = list(string)
  description = "description"
}
##########igw#########
variable "igw" {
  description = "specify tag"
  type        = string
  default     = "igw-1"
}
variable "vpc_igw" {
  description = "specify tag"
}
##############rtb############
variable "vpc_rtb" {
  description = "specify tag"
}
variable "rtb_name" {
  description = "route table name"
  type        = string
  default     = "public-rtb"
}
variable "routes" {
  description = "routes"
  type        = string
  default     = "0.0.0.0/0"
}
variable "igw_gateway" {
  description = "igw_gateway"
}
variable "nat_id" {
  default     = ""
  description = "description"
}

variable "private_route_table_tag" {
  default     = ""
  description = "description"
}

variable "eip_tag" {
  description = "description"
}


variable "nat_tag" {
  description = "description"
}
######################pulic instance ###############################

variable "pub_ami" {
  description = "description"
}

variable "pub_instance_type" {
  default     = " "
  description = "description"
}

variable "pub_subnet_id" {
  type        = string
  default     = " "
  description = "description"
}

variable "key_name" {
  type        = string
  default     = " "
  description = "description"
}

variable "pub_instance_tag" {
  default     = " "
  description = "description"
}
########################private instance ########################
variable "pri_ami" {
  type        = list(string)
  description = "description"
}

variable "pri_instance_type" {
  type        = list(string)
  description = "description"
}

variable "pri_subnet_id" {
  type    = list(string)
  default = [""]
}

variable "pri_instance_tag" {
  type        = list(string)
  description = "description"
}

############################ security group ##########################
variable "sg_tag" {
  type        = list(string)
  description = "description"
}

variable "sg_cidr_block" {
  type        = list(string)
  description = "description"
}
variable "security_group_id" {
  type    = list(string)
  default = [""]
}
#########################ASG ###############################################

variable "grafana_template" {
  type = string
  description = "template name"  
}
variable "launch_ami" {
  type = string
  description = "launch_ami"  
}
variable "instance" {
  type = string
  description = "instance"  
}

variable "desired_capacity" {
  type = string
  description = "desired_capacity"  
}
variable "max_size" {
  type = string
  description = "template name"  
} 
variable "min_size" {
  type = string
  description = "template name"  
}

variable "availability_zones" {
 type = list(string)
 default = [ "us-east-1" ]
}

####################### nacl ######################################
variable "vpc_id" {
  default     = ""
  description = "description"
}

variable "protocol" {
  default     = ""
  description = "description"
}

variable "rule_no" {
    type = number
  description = "description"
}

variable "action" {
  default     = ""
  description = "description"
}

variable "nacl_cidr_block" {
  default     = ""
  description = "description"
}

variable "from_port" {
  type = number
  description = "description"
}

variable "to_port" {
  type = number
  description = "description"
}

variable "nacl_tag" {
  default     = ""
  description = "description"
}